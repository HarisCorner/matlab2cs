/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_addThem_mex.c
 *
 * Code generation for function '_coder_addThem_mex'
 *
 */

/* Include files */
#include "_coder_addThem_api.h"
#include "_coder_addThem_mex.h"

/* Function Declarations */
static void addThem_mexFunction(int32_T nlhs, int32_T nrhs, const mxArray *prhs
  [2]);

/* Function Definitions */
static void addThem_mexFunction(int32_T nlhs, int32_T nrhs, const mxArray *prhs
  [2])
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 2) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 2, 4, 7,
                        "addThem");
  }

  if (nlhs > 0) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 7,
                        "addThem");
  }

  /* Call the function. */
  addThem_api(prhs, nlhs);

  /* Module termination. */
  addThem_terminate();
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  (void)plhs;
  mexAtExit(addThem_atexit);

  /* Initialize the memory manager. */
  /* Module initialization. */
  addThem_initialize();

  /* Dispatch the entry-point. */
  addThem_mexFunction(nlhs, nrhs, prhs);
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_addThem_mex.c) */
