/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.c
 *
 * Code generation for function 'addThem'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"

/* Function Definitions */
void addThem(Double a, Double b)
{
  (void)a;
  (void)b;
}

/* End of code generation (addThem.c) */
