/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.h
 *
 * Code generation for function 'addThem'
 *
 */

#ifndef ADDTHEM_H
#define ADDTHEM_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "addThem_types.h"

/* Function Declarations */
extern Double addThem(Double a, Double b);

#endif

/* End of code generation (addThem.h) */
