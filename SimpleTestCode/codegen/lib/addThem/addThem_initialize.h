/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem_initialize.h
 *
 * Code generation for function 'addThem_initialize'
 *
 */

#ifndef ADDTHEM_INITIALIZE_H
#define ADDTHEM_INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "addThem_types.h"

/* Function Declarations */
extern void addThem_initialize(void);

#endif

/* End of code generation (addThem_initialize.h) */
