/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.c
 *
 * Code generation for function 'addThem'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"

/* Function Definitions */
double addThem(double a, double b)
{
  double c=0D;
  c = b+a;
  return c;
}

/* End of code generation (addThem.c) */
