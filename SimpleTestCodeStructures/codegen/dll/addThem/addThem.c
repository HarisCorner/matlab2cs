/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.c
 *
 * Code generation for function 'addThem'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"

/* Function Definitions */
void addThem(struct0_T *RISTGF, struct0_T *LISTGF)
{
  RISTGF->LocTO = 5;
  RISTGF->LocHS = 9;
  LISTGF->LocTO = 15;
  LISTGF->LocHS = 19;
}

/* End of code generation (addThem.c) */
