/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_addThem_api.c
 *
 * Code generation for function '_coder_addThem_api'
 *
 */

/* Include files */
#include "tmwtypes.h"
#include "_coder_addThem_api.h"
#include "_coder_addThem_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131466U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "addThem",                           /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static struct0_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static struct0_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *RISTGF,
  const char_T *identifier);
static const mxArray *emlrt_marshallOut(const struct0_T u);

/* Function Definitions */
static struct0_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[2] = { "LocTO", "LocHS" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 2, fieldNames, 0U, &dims);
  thisId.fIdentifier = "LocTO";
  y.LocTO = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0,
    "LocTO")), &thisId);
  thisId.fIdentifier = "LocHS";
  y.LocHS = c_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1,
    "LocHS")), &thisId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = d_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static struct0_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *RISTGF,
  const char_T *identifier)
{
  struct0_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(RISTGF), &thisId);
  emlrtDestroyArray(&RISTGF);
  return y;
}

static const mxArray *emlrt_marshallOut(const struct0_T u)
{
  const mxArray *y;
  static const char * sv0[2] = { "LocTO", "LocHS" };

  const mxArray *b_y;
  const mxArray *m0;
  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 2, sv0));
  b_y = NULL;
  m0 = emlrtCreateDoubleScalar(u.LocTO);
  emlrtAssign(&b_y, m0);
  emlrtSetFieldR2017b(y, 0, "LocTO", b_y, 0);
  b_y = NULL;
  m0 = emlrtCreateDoubleScalar(u.LocHS);
  emlrtAssign(&b_y, m0);
  emlrtSetFieldR2017b(y, 0, "LocHS", b_y, 1);
  return y;
}

void addThem_api(const mxArray * const prhs[2], int32_T nlhs, const mxArray
                 *plhs[2])
{
  struct0_T RISTGF;
  struct0_T LISTGF;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  RISTGF = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "RISTGF");
  LISTGF = emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "LISTGF");

  /* Invoke the target function */
  addThem(&RISTGF, &LISTGF);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(RISTGF);
  if (nlhs > 1) {
    plhs[1] = emlrt_marshallOut(LISTGF);
  }
}

void addThem_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  addThem_xil_terminate();
}

void addThem_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

void addThem_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (_coder_addThem_api.c) */
