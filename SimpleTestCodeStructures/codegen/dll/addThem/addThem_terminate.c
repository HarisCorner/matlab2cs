/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem_terminate.c
 *
 * Code generation for function 'addThem_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"
#include "addThem_terminate.h"

/* Function Definitions */
void addThem_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (addThem_terminate.c) */
