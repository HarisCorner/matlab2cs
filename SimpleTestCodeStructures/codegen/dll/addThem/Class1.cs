﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
/* C# code */
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct struct0_T
{
    public double LocTO;
    public double LocHS;
    //[FieldOffset(8)] public int right;
    //[FieldOffset(12)] public int bottom;
}

class Program
{
    [DllImport(@"C:\MyGitDriveGTX\GTRMigrateToCS\SimpleTestCode3\codegen\dll\addThem\addThem.dll",
             CallingConvention = CallingConvention.Cdecl)]
    public static extern void addThem(ref struct0_T RISTGF, ref struct0_T LISTGF);

 

    static void Main(string[] args)
    {

        struct0_T RISTGF, LISTGF;

        RISTGF.LocTO = 7;
        RISTGF.LocHS = 9d;

        LISTGF.LocTO = 6;
        LISTGF.LocHS = 8;

        Console.WriteLine("The default numbers are");
        Console.WriteLine(RISTGF.LocHS);
        Console.WriteLine(LISTGF.LocTO);

        addThem(ref RISTGF, ref LISTGF);

        Console.WriteLine("The numbers after calling the MATLAB function are:");
        Console.WriteLine(RISTGF.LocHS);
        Console.WriteLine(LISTGF.LocTO);

        //Console.WriteLine(c);
        Thread.Sleep(3000);
        
    }
}
