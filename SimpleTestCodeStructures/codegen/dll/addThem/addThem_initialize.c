/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem_initialize.c
 *
 * Code generation for function 'addThem_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"
#include "addThem_initialize.h"

/* Function Definitions */
void addThem_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (addThem_initialize.c) */
