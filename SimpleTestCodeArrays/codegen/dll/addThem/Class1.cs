﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
/* C# code */
using System.Runtime.InteropServices;

class Program
{
    [DllImport(@"C:\MyGitDriveGTX\GTRMigrateToCS\SimpleTestCodeArrays\codegen\dll\addThem\addThem.dll",
             CallingConvention = CallingConvention.Cdecl)]
    public static extern Double addThem(Double[] a, Double[] b, Double[] c);

    static void Main(string[] args)
    {

        Double[] a = new Double[5];
        Double[] b = new Double[5];

        a[0] = 9;
        b[0] = 17;
        a[1] = 5;
        b[1] = 100;

        Console.WriteLine("The default values are:");
        Console.WriteLine(a[0]);
        Console.WriteLine(b[0]);
        Console.WriteLine(a[1]);
        Console.WriteLine(b[1]);

        Double[] c = new Double[5];

        addThem(a, b, c);

        Console.WriteLine("The sum after calling the MATLAB function are:");
        Console.WriteLine(c[0]);
        Console.WriteLine(c[1]);
        Thread.Sleep(3000);

    }
}
