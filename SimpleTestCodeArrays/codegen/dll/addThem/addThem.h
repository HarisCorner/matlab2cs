/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.h
 *
 * Code generation for function 'addThem'
 *
 */

#ifndef ADDTHEM_H
#define ADDTHEM_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "addThem_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void addThem(const double a[2], const double b[2], double c[2]);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (addThem.h) */
