/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem.c
 *
 * Code generation for function 'addThem'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "addThem.h"

/* Function Definitions */
void addThem(const double a[2], const double b[2], double c[2])
{
  c[0] = a[0] + b[0];
  c[1] = a[1] + b[1];
}

/* End of code generation (addThem.c) */
