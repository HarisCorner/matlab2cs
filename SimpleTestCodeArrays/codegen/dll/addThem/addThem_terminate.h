/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * addThem_terminate.h
 *
 * Code generation for function 'addThem_terminate'
 *
 */

#ifndef ADDTHEM_TERMINATE_H
#define ADDTHEM_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "addThem_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void addThem_terminate(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (addThem_terminate.h) */
